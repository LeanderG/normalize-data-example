import org.apache.commons.math3.linear.BlockRealMatrix
import org.apache.commons.math3.stat.StatUtils
import java.io.File

fun main(args: Array<String>) {

    // constants
    val delimiter = ","
    val inputPath = "data/seeds.csv"
    val outputPath = "output/seeds_normalized.csv"
    
    // read input file
    val bufferedReader = File(inputPath).bufferedReader()
    val inputString = bufferedReader.use { it.readText() }
            .removeUTF8BOM() // toDouble() will not work if there is a BOM and it's not removed

    // get lines, number of rows and columns
    val lines = inputString.lines()
            .filter { it != "" } // check if there is an empty line and remove it
    val numberOfRows = lines.size
    val numberOfColumns = lines[0].split(delimiter).size

    // instantiate matrix with correct number of rows and columns
    val blockRealMatrix = BlockRealMatrix(numberOfRows, numberOfColumns)

    // populate the matrix
    lines.forEachIndexed { index, value ->
        println(value)
        val row = value
                .split(delimiter)
                .map {
                    it.toDouble()
                }
                .toDoubleArray()
        blockRealMatrix.setRow(index, row)
    }

    // normalize columns
    for (i in 0 until numberOfColumns) {
        val normalizedColumn = StatUtils.normalize(blockRealMatrix.getColumn(i))
        blockRealMatrix.setColumn(i, normalizedColumn)
    }

    // transform matrix to a string
    val normalizedOutput = blockRealMatrix.data.joinToString("\r\n") {
        it.joinToString(",")
    }

    // write the string to an output file
    val bufferedWriter = File(outputPath).bufferedWriter()
    bufferedWriter.use { it.write(normalizedOutput) }
}

// remove Byte Order Mark (BOM)
fun String.removeUTF8BOM() = if (this.startsWith("\uFEFF")) { this.substring(1) } else { this }