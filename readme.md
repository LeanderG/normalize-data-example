### Shows how to normalize a data set with Kotlin
Normalize means subtract the expected value from the columns and divide them by their standard deviations.
I'm using The Apache Commons Mathematics Library to do this.